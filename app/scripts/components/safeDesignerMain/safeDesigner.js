'use strict';
angular.module('app')
    .controller('SafeDesigner', function($scope, safeDesignerAPI, commonService, $uibModal, $compile, $filter, $translate, $interpolate) {

        var safeDesignerVm = this;
        var customModelObj = "customModelObj";
        var downloadFileName = "";
        safeDesignerVm.isModalOpen = false;
        safeDesignerVm.listOfFieldsInUploadedFile = []; // total no of fields in the uploaded html
        safeDesignerVm.listOfAvailableFields = []; // total no of fields that can be added to the form
        safeDesignerVm.arrOfDataToDir = [];

        safeDesignerVm.fileData = '';
        $scope.showDownloadBtn = false;

        /**
          called when the html file is uploaded.
        */
        safeDesignerVm.loadFile = function(event) {
            if ((event.target.files[0].name).indexOf("employer") > -1) {
                safeDesignerAPI.apiUrl = './../data/employee_details.json';
                customModelObj = "location";
                downloadFileName = "location";
            } else if ((event.target.files[0].name).indexOf("location") > -1) {
                safeDesignerAPI.apiUrl = './../data/location_details.json';
                customModelObj = '$ctrl.parentCtrl.employer';
                downloadFileName = "employerDetails";
            } else if ((event.target.files[0].name).indexOf("profile") > -1) {
                safeDesignerAPI.apiUrl = './../data/profile_details.json';
                customModelObj = 'personData';
                downloadFileName = "profile";
            }
            var resp = safeDesignerAPI.getApiDetailsJson();
            resp.then(function(response) {
                safeDesignerVm.empDetailsJson = response.data;
                safeDesignerVm.empDetailsJsonCopy = angular.copy(safeDesignerVm.data);
                safeDesignerVm.createNewJson(safeDesignerVm.empDetailsJson);
            });
            $scope.$apply(function() {
                $scope.showDownloadBtn = true;
            });
            let file = event.target.files[0];
            let reader = new FileReader();

            reader.onload = function(event) {
                document.getElementById('formToEdit').innerHTML = event.target.result;
            };
            reader.onloadend = safeDesignerVm.getFormElements;
            reader.readAsText(file);
        };

        /**
          Binds event listeners to all the form elements
        */
        safeDesignerVm.getFormElements = function() {
            safeDesignerVm.getTextIpsAndBind();
            safeDesignerVm.getLabelsAndBind();
            safeDesignerVm.getDropdownsAndBind();
            safeDesignerVm.getListOfAvailableFields();
            safeDesignerVm.getFormHeadingDivAddClass();
            $scope.safeDesignerVm = safeDesignerVm;

            safeDesignerVm.addDragNDrop();
        };
        /**
        Make all the input fields draggable .
        */
        safeDesignerVm.addDragNDrop = function() {
            var formElements = angular.element(document.getElementsByClassName('form-group'));
            angular.forEach(formElements, function(value) {
                angular.element(value).attr('draggable', true);
                angular.element(value).on('dragstart', safeDesignerVm.drag);
            });
            angular.element(document.getElementById('formToEdit')).on('dragover', allowDrop);
            angular.element(document.getElementById('formToEdit')).on('drop', drop);
        };

        function allowDrop(ev) {
            ev.preventDefault();
        }

        safeDesignerVm.drag = function(event) {
            safeDesignerVm.droppableField = event.target;
        }

        function drop(ev) {
            ev.preventDefault();
            let data = safeDesignerVm.droppableField;
            if (data.tagName === "LI" && data.textContent === "Add image") {
                let field = { controllerType: 'image' };
                let dataDetails = commonService.getFieldDetails(field);
                if ($(event.target).closest('.personnel-header').length > 0) {
                    $($(event.target).closest('.personnel-header .row div')).before($compile(dataDetails)($scope));
                } else if ($(event.target).closest('.form-group').length > 0) {
                    $($(event.target).closest('.form-group')[0]).before($compile(dataDetails.replace("form-group col-sm-6", "form-group"))($scope));
                } else if ($(event.target).closest('.form-heading ').length > 0) {
                    $($(event.target).closest('.form-heading')[0]).before($compile(dataDetails)($scope));
                }

                $scope.$apply();
            } else if (data.tagName === "LI" && $(event.target).closest('.form-group').length > 0) {
                let fields = $filter('filter')(safeDesignerVm.listOfFields, { labelTxt: data.textContent });
                let index = safeDesignerVm.findIndexOfObjInArr(safeDesignerVm.listOfAvailableFields, fields[0].id);
                safeDesignerVm.listOfAvailableFields.splice(index, 1);
                let dataDetails = commonService.getFieldDetails(fields[0]);
                $($(event.target).closest('.form-group')[0]).before($compile(dataDetails)($scope));
                $scope.$apply();
            } else {
                if ($(event.target).closest('.form-group').length > 0 && $(event.target).closest('.form-group')[0].innerText != $(safeDesignerVm.droppableField).closest('.form-group')[0].innerText) {
                    safeDesignerVm.droppableField.parentNode.removeChild(safeDesignerVm.droppableField);
                    $($(event.target).closest('.form-group')[0]).before(data);
                }
            }

        }


        /**
             Binds event listeners to all the input texts in the form
        */
        safeDesignerVm.getTextIpsAndBind = function() {
            // safeDesignerVm.allTextInp = angular.element(document).find('input[type=text]');
            safeDesignerVm.allTextInp = document.querySelectorAll('input[type=text]');

            angular.forEach(safeDesignerVm.allTextInp, function(value) {
                angular.element(value).on('dblclick', safeDesignerVm.showTextInpDetails);
                let field = $filter('filter')(safeDesignerVm.listOfFields, { "id": value.getAttribute('id') }, true);
                if (field && field[0] && field[0].mandatoryField) {
                    angular.element(value).attr('required', field[0].mandatoryField);
                }
                $compile(value)($scope);
            });
        };

        /**
          Binds event listeners to all the labels in the form
        */
        safeDesignerVm.getLabelsAndBind = function() {
            // safeDesignerVm.labels = angular.element(document).find('LABEL');
            safeDesignerVm.labels = angular.element(document.getElementsByTagName('LABEL'));

            angular.forEach(safeDesignerVm.labels, function(value) {
                let spn = angular.element('<span></span>');
                spn.attr('class', 'pull-right glyphicon glyphicon-remove delGlph');
                spn.attr('data-id', value.getAttribute('for'));
                angular.element(spn).on('click', safeDesignerVm.deleteElements);
                value.parentNode.insertBefore(spn[0], value);

                angular.element(value).on('dblclick', safeDesignerVm.showLabelDetails);
                let field = $filter('filter')(safeDesignerVm.listOfFields, { "id": value.getAttribute('for') });
                safeDesignerVm.listOfFieldsInUploadedFile.push(field[0]);
            });
        };

        /**
          Binds event listeners to all the dropdowns in the form
        */
        safeDesignerVm.getDropdownsAndBind = function() {
            safeDesignerVm.dropdowns = document.getElementsByTagName('SELECT');
            angular.forEach(safeDesignerVm.dropdowns, function(value) {
                angular.element(value).on('click', safeDesignerVm.showDropdownDetails);

                let dropdownField = $filter('filter')(safeDesignerVm.listOfFields, { "id": value.id });
                safeDesignerVm.listOfFieldsInUploadedFile.push(dropdownField[0]);
            });
        };

        safeDesignerVm.getListOfAvailableFields = function() {
            angular.forEach(safeDesignerVm.listOfFields, function(value) {
                let field = $filter('filter')(safeDesignerVm.listOfFieldsInUploadedFile, { "id": value.id })
                if (field.length === 0) {
                    safeDesignerVm.listOfAvailableFields.push(value);
                }
            });
            $scope.$digest();
        };

        /**
             Grouping of field as per headers
        */
        safeDesignerVm.getFormHeadingDivAddClass = function() {
            safeDesignerVm.formHdg = angular.element(document).find('.form-heading');
            angular.forEach(safeDesignerVm.formHdg, function(value) {
                value.classList.add('wrap');
                value.style.margin = "55px 5px 0 5px";
            });
        };

        /**
          open modal to show all input text properties which can be edited
        */
        safeDesignerVm.showTextInpDetails = function() {
            let field = $filter('filter')(safeDesignerVm.listOfFields, { id: event.target.id }, true);
            if (!safeDesignerVm.isModalOpen) {
                safeDesignerVm.isModalOpen = true;
                let uibModal = $uibModal.open({
                    animation: false,
                    templateUrl: './scripts/components/textInpProp/textInpProp.html',
                    controller: 'TextInpPropController',
                    controllerAs: 'TextInpPropCtrl',
                    size: 'md',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        data: event,
                        fieldDetails: field[0]
                    }
                });
                let ev = event;

                uibModal.result.then(function(data) {
                    safeDesignerVm.isModalOpen = false;
                    if (data) {
                        ev.target.required = data.required;
                        ev.target.placeholder = data.placeholder;
                        if (data.maxLength) {
                            ev.target.maxLength = data.maxLength;
                        } else {
                            ev.target.removeAttribute('maxLength');
                        }

                        if (data.ngPattern !== undefined && data.ngPattern !== null) {
                            ev.target.setAttribute('ng-pattern', data.ngPattern);
                        }
                        if (data.controllerType !== field[0].controllerType && !(data.controllerType === "text" && field[0].controllerType === "number")) {
                            var elem = commonService.changeFieldType(data, field[0]);
                            angular.element(ev.target).replaceWith($compile(elem)($scope));
                        } else {
                            $compile(ev.target)($scope);
                        }
                    }
                });
            }

        };

        /**
          open modal to show all label properties which can be edited
        */
        safeDesignerVm.showLabelDetails = function() {
            let uibModal = $uibModal.open({
                animation: false,
                templateUrl: './scripts/components/labelProp/labelProp.html',
                controller: 'LabelPropController',
                controllerAs: 'LabelPropCtrl',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    data: event
                }
            });
            event.stopPropagation();
            event.preventDefault();
            let ev = event;

            uibModal.result.then(function(data) {
                if (data) {
                    ev.target.innerHTML = data;
                }

            });
        };

        /**
          open modal to show all dropdown properties which can be edited
        */
        safeDesignerVm.showDropdownDetails = function() {
            let field = $filter('filter')(safeDesignerVm.listOfFields, { id: event.target.id }, true);
            let uibModal = $uibModal.open({
                animation: false,
                templateUrl: './scripts/components/dropdownProp/dropdownProp.html',
                controller: 'DropdownPropController',
                controllerAs: 'DrpdwnPropCtrl',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    data: event,
                    fieldDetails: field[0]
                }
            });
            let ev = event;

            uibModal.result.then(function(data) {
                if (data) {
                    ev.target.required = data.required;
                    ev.target.options[0].innerHTML = data.defaultOption;
                    if (data.controllerType !== "dropdown") {
                        var elem = commonService.changeFieldType(data, field[0]);
                        angular.element(ev.target.parentNode).replaceWith($compile(elem)($scope));
                    } else {
                        $compile(ev.target)($scope);
                    }
                }
            });
        };

        /**
          used to download the edited form
        */
        safeDesignerVm.downloadAsHtml = function() {
            let uibModal = $uibModal.open({
                animation: false,
                templateUrl: './scripts/components/downloadDoc/downloadDoc.html',
                controller: 'DownloadDocController',
                controllerAs: 'DownloadCtrl',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    data: { defaultName: downloadFileName }
                }
            });
            uibModal.result.then(function(data) {
                if (data) {
                    safeDesignerVm.removeDeleteGlyphicons();
                    document.getElementById('formToEdit').innerHTML = document.getElementById('formToEdit').innerHTML.replace(/<!--[\s\S]*?-->/g, '');
                    angular.element(document.getElementsByClassName('form-group')).parent().remove;
                    // let fileToSave = document.getElementById('formToEdit').innerHTML;
                    let fileToSave = (angular.element(document.getElementById('formToEdit')))[0].innerHTML;
                    let createFileBlob = new Blob([fileToSave], { type: 'text/html' }); //text/plain
                    let blobFileURL = window.URL.createObjectURL(createFileBlob);
                    let fileName = data;

                    let link = angular.element('<a></a>');
                    link.attr('download', fileName);
                    link.attr('href', blobFileURL);
                    document.body.appendChild(link[0]);

                    link[0].click();
                    window.URL.revokeObjectURL(blobFileURL);
                    safeDesignerVm.addDeleteGlyphicons();
                }
            });
        };

        /**
          used to delete elements in the form
        */
        safeDesignerVm.deleteElements = function() {
            let attrVal = event.target.getAttribute('data-id');
            if (attrVal === "image") {
                angular.element(event.target.parentNode.parentNode).remove();
            } else {
                angular.element(event.target.parentNode).remove();
            }

            let field = $filter('filter')(safeDesignerVm.listOfFields, { id: attrVal });
            if (field.length > 0) {
                safeDesignerVm.listOfAvailableFields.push(field[0]);

                let index = safeDesignerVm.findIndexOfObjInArr(safeDesignerVm.arrOfDataToDir, field[0].id);
                if (index > -1) {
                    safeDesignerVm.arrOfDataToDir.splice(index, 1);
                }
            }
            if (!$scope.$$phase) {
                $scope.$digest();
            }
        };

        /**
          used to remove delete glyphicons from the dom before downloading
        */
        safeDesignerVm.removeDeleteGlyphicons = function() {
            let delGlphs = angular.element(document.getElementsByClassName('delGlph'));
            angular.forEach(delGlphs, function(value) {
                angular.element(value).remove();
            });
        };

        /**
          used to add delete glyphicons to the dom after downloading
        */
        safeDesignerVm.addDeleteGlyphicons = function() {
            safeDesignerVm.labels = angular.element(document).find('LABEL');

            angular.forEach(safeDesignerVm.labels, function(value) {
                let spn = angular.element('<span></span>');
                spn.attr('class', 'pull-right glyphicon glyphicon-remove delGlph');
                spn.attr('data-id', value.getAttribute('for'));
                angular.element(spn).on('click', safeDesignerVm.deleteElements);
                value.parentNode.insertBefore(spn[0], value);
            });
        };

        safeDesignerVm.findIndexOfObjInArr = function(list, id) {
            for (let i = 0; i < list.length; i++) {
                if (list[i].id === id) {
                    return i;
                }
            }
        };


        /**
          add the default attributes to update the json format
        */
        safeDesignerVm.createNewJson = function(empDetailsJson) {
            angular.forEach(empDetailsJson, function(value) {
                value.controllerType = value.dataType;
                value.labelTxt = value.fieldName;
                value.id = value.fieldName;
                value.required = value.mandatoryField;
                value.placeholder = value.fieldName;
                value.ngModel = customModelObj + "." + value.fieldName;
                value.validation = {};
                value.defaultOptionVal = value.fieldName;
                value.defaultOptionIsDisabled = true;
                value.ngOptions = '';
            })
            safeDesignerVm.listOfFields = empDetailsJson;
        };

    });