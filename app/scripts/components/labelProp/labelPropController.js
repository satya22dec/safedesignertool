'use strict';
angular.module('app')
    .controller('LabelPropController', function($scope, $uibModalInstance, data) {
        var labelVm = this;

        labelVm.fnInit = function() {
            labelVm.data = data;
            labelVm.textValue = labelVm.data.target.innerHTML;
        };

        labelVm.closeModal = function() {
            $uibModalInstance.close();
        };
        labelVm.save = function() {
            $uibModalInstance.close(labelVm.textValue);
        };

        labelVm.fnInit();
    });