'use strict';

angular.module('app')
    .controller('DropdownPropController', function($scope, $uibModalInstance, data, fieldDetails) {
        var dropdownVm = this;

        dropdownVm.fnInit = function() {
            dropdownVm.data = data;
            dropdownVm.prop = {};
            dropdownVm.prop.mandatoryField = fieldDetails ? fieldDetails.mandatoryField : false;
            dropdownVm.prop.required = dropdownVm.data.target.required;
            dropdownVm.prop.defaultOption = dropdownVm.data.target.options[0].innerHTML;
            dropdownVm.prop.controllerType = "dropdown";
            if (fieldDetails.dataType === 'number') {
                dropdownVm.prop.controllerArray = ["dropdown", "text", "phone"];
            } else {
                dropdownVm.prop.controllerArray = ["dropdown", "text"];
            }
        };
        dropdownVm.closeModal = function() {
            $uibModalInstance.close();
        };
        dropdownVm.save = function() {
            $uibModalInstance.close(dropdownVm.prop);
        };

        dropdownVm.fnInit();
    });