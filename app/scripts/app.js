'use strict';


angular.module('app', [
        'ui.router',
        'ui.bootstrap',
        'pascalprecht.translate'
    ])
    .config(['$stateProvider', '$urlRouterProvider', '$translateProvider', function($stateProvider, $urlRouterProvider, $translateProvider) {

        $stateProvider.state('safeDesigner', {
            url: '/',
            templateUrl: 'scripts/components/safeDesignerMain/safeDesigner.html',
            controller: 'SafeDesigner',
            controllerAs: 'safeDesnCtlr'
        });
        $urlRouterProvider.otherwise('/');
    }]);