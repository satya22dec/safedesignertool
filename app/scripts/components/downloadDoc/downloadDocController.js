'use strict';
angular.module('app')
    .controller('DownloadDocController', function($scope, $uibModalInstance, data) {
        var downloadVm = this;

        downloadVm.fnInit = function() {
            downloadVm.textValue = data.defaultName;
        };

        downloadVm.cancel = function() {
            $uibModalInstance.close();
        };
        downloadVm.download = function() {
            $uibModalInstance.close(downloadVm.textValue);
        };

        downloadVm.fnInit();
    });