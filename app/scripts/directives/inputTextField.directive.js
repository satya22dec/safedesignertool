'use strict';

angular.module('app')
    .directive('inputfield', function() {
        return {
            restrict: 'E',
            scope: {
                data: '@'
            },
            link: function(scope, element) {
                /* console.log('directive');
                 console.log(scope.data);*/
            },
            template: `<div >
                Hello {{data}}
                </div>`
        };
    });
// data:'=',
//         deleteElements:'&',
//         showLabelDetails:'&',
//         showTextInpDetails:'&'

// template:`<div data-ng-repeat="field in data">
//                 <div class="form-group col-xs-6">
//                   <span class="pull-right glyphicon glyphicon-remove delGlph" data-id={{field.id}} ng-click="deleteElements()"></span>
//                   <label for="{{field.id}}" class="form-label"  ng-click="showLabelDetails()">{{field.labelTxt}}</label>
//                   <input type="text" placeholder="Placeholder" id="{{field.id}}" class="form-control" ng-click="showTextInpDetails()"
//                     name="test" ng-model="test">
//                 </div>
//                 </div>`

/*data.ngModel {{data.labelTxt}} Field Name
link: function(scope, element) {
                  var generatedTemplate = 'Testing : <input type="test" />';
                  element.append($compile(generatedTemplate)(scope));
              }*/