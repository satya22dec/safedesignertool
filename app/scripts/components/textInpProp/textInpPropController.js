'use strict';
angular.module('app')
    .controller('TextInpPropController', function($scope, $uibModalInstance, data, fieldDetails) {
        var textInpVm = this;

        textInpVm.fnInit = function() {
            textInpVm.data = data;
            textInpVm.prop = {};
            textInpVm.prop.maxSize = fieldDetails ? fieldDetails.size : null;
            textInpVm.prop.maxLength = textInpVm.data.target.maxLength < 0 ? textInpVm.prop.maxSize : textInpVm.data.target.maxLength;
            textInpVm.prop.mandatoryField = fieldDetails ? fieldDetails.mandatoryField : false;
            textInpVm.prop.required = textInpVm.data.target.required;
            textInpVm.prop.placeholder = textInpVm.data.target.placeholder;
            if (textInpVm.data.target.attributes['ng-pattern']) {
                textInpVm.prop.ngPattern = textInpVm.data.target.attributes['ng-pattern'].value;
            }
            textInpVm.prop.controllerType = "text";
            // textInpVm.prop.controllerArray = ["dropdown", "text", "phone"];
            if (!fieldDetails && (textInpVm.data.target.id.includes('Phone') || textInpVm.data.target.id.includes('Fax'))) {
                textInpVm.prop.controllerArray = ["text"];
            } else if (fieldDetails.dataType === 'number') {
                textInpVm.prop.controllerArray = ["dropdown", "text", "phone"];
            } else if (fieldDetails.dataType === 'date') {
                textInpVm.prop.controllerType = "date";
                textInpVm.prop.controllerArray = ["date"];
            } else {
                textInpVm.prop.controllerArray = ["dropdown", "text"];
            }

            //console.log(textInpVm.data.target.attributes['ng-pattern'].value);
            /*textInpVm.prop.patternValidation = [{desc : 'Only Numbers allowed', value:'/^[0-9]$/'},
                  {desc : 'Only alphabets allowed', value:'/^[a-zA-Z]$/'},
                  {desc : 'Only alphanumerics allowed', value:'/^[a-zA-Z0-9]$/'}];*/
            //textInpVm.prop.maxLength = textInpVm.data.target.maxlength < 0 ? '' : textInpVm.data.target.maxLength ;
        };

        textInpVm.closeModal = function() {
            $uibModalInstance.close();
        };
        textInpVm.save = function() {
            $uibModalInstance.close(textInpVm.prop);
        };
        textInpVm.fnInit();
    });