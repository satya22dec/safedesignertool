"use strict";

angular.module('app')
    .service('safeDesignerAPI', ['$http', function($http) {
        this.apiUrl = './../data/employee_details.json';

        this.getApiDetailsJson = function() {
            var promise = $http({
                url: this.apiUrl,
                method: 'GET'
            });
            return promise;
        };


    }]);