"use strict";

angular.module('app')
    .service('commonService', ['$http', '$interpolate', function($http, $interpolate) {

        this.changeFieldType = function(updatedData, eventDeatils) {
            if (updatedData.controllerType === "text") {
                var inp = `<input type="text"  placeholder="{{eventDeatils.id}}"  id="{{eventDeatils.id}}" class="form-control" ng-dblClick="safeDesignerVm.showTextInpDetails()" name="test" autofocus ng-model="{{eventDeatils.ngModel}}">`;
            } else if (updatedData.controllerType === "dropdown") {
                var inp = `<div class="select-wrap">
                        <select class="form-control" autofocus  id="{{eventDeatils.id}}" ng-model="{{eventDeatils.ngModel}}" ng-click="safeDesignerVm.showDropdownDetails()" >
                          <option value="" >select options</option>
                        </select>
                      </div>`;
            } else if (updatedData.controllerType === "phone") {
                var inp = `<div id="{{eventDeatils.id}}" class="input-group" >
                    <span class="input-group-addon plus-icon">+</span>
                    <input type="text" id="PhoneExt" class="form-control" ng-dblClick="safeDesignerVm.showTextInpDetails()" numbers-only="true" name="PhoneExt" placeholder="" ng-pattern="/^[0-9]{0,3}$/" maxlength="3" ng-model="$ctrl.parentCtrl.tempObj.Phone.PhoneExt" />
                    <span class="input-group-addon addon-css">-</span>
                    <input type="text" id="Phone1" class="form-control" ng-dblClick="safeDesignerVm.showTextInpDetails()" numbers-only="true" name="phone1" placeholder="" ng-pattern="/^[0-9]{3}$/" maxlength="3" ng-model="$ctrl.parentCtrl.tempObj.Phone.Phone1" required />
                    <span class="input-group-addon addon-css">-</span>
                    <input type="text" id="Phone2" class="form-control" ng-dblClick="safeDesignerVm.showTextInpDetails()"  numbers-only="true" name="phone2" placeholder="" ng-pattern="/^[0-9]{3}$/" maxlength="3" ng-model="$ctrl.parentCtrl.tempObj.Phone.Phone2" required />
                    <span class="input-group-addon addon-css">-</span>
                    <input type="text" id="Phone3" class="form-control" ng-dblClick="safeDesignerVm.showTextInpDetails()" numbers-only="true" name="phone3" placeholder="" ng-pattern="/^[0-9]{4}$/" maxlength="4" ng-model="$ctrl.parentCtrl.tempObj.Phone.Phone3" required />
                  </div>`;
            } else if (updatedData.controllerType === "date") {
                var inp = `<div class="custom-angularstrap">
                        <input size="10" id="{{eventDeatils.id}}"   type="text" class="form-control" data-autoclose="1" placeholder="" ng-model="{{eventDeatils.ngModel}}" data-date-format="MMM dd, yyyy" bs-datepicker ng-change="$ctrl.dateValidation()" ng-dblClick="safeDesignerVm.showTextInpDetails()" autocomplete="off">
                    </div>
                    <span class="date-icon form-control-feedback"></span>`;
            }
            return $interpolate(inp)({ eventDeatils: eventDeatils });
        };


        /**
             Returns draggable element details as per thr controller type
        */
        this.getFieldDetails = function(field) {
            let inp = "";
            if (field.controllerType === 'text' || field.controllerType === 'number') {
                inp = `
                <div class="form-group " draggable="true" onDragstart="angular.element(this).scope().safeDesnCtlr.drag(event)" >
                    <span class="pull-right glyphicon glyphicon-remove delGlph" data-id={{field.id}} ng-click="safeDesignerVm.deleteElements()"></span>
                    <label for="{{field.id}}" class="form-label"  ng-dblClick="safeDesignerVm.showLabelDetails()">{{field.labelTxt}}</label>
                    <input type="text"  placeholder="Placeholder" id="{{field.id}}" class="form-control" ng-dblClick="safeDesignerVm.showTextInpDetails()" name="test" ng-model="{{field.ngModel}}">
                    </div>`;
            } else if (field.controllerType === 'date') {
                inp = `
                <div class="form-group " draggable="true" onDragstart="angular.element(this).scope().safeDesnCtlr.drag(event)">
                <span class="pull-right glyphicon glyphicon-remove delGlph" data-id={{field.id}} ng-click="safeDesignerVm.deleteElements()"></span>
                    <label for="{{field.id}}" class="form-label" ng-dblClick="safeDesignerVm.showLabelDetails()">{{field.labelTxt}}</label>

                    <div class="custom-angularstrap">
                        <input size="10" type="text" id="{{field.id}}"   class="form-control" data-autoclose="1" placeholder="" ng-model="{{field.ngModel}}" data-date-format="MMM dd, yyyy" bs-datepicker ng-change="$ctrl.dateValidation()" ng-dblClick="safeDesignerVm.showTextInpDetails()" autocomplete="off">
                    </div>
                    <span class="date-icon form-control-feedback"></span>
                </div>`;
            } else if (field.controllerType === 'Phone') {
                inp = `
                 <div class="form-group " draggable="true" onDragstart="angular.element(this).scope().safeDesnCtlr.drag(event)" >
                  <span class="pull-right glyphicon glyphicon-remove delGlph" data-id={{field.id}} ng-click="safeDesignerVm.deleteElements()"></span>
                  <label for="Phone" class="form-label required-label">{{field.labelTxt}}</label>
                  <div id="Phone" class="input-group">
                    <span class="input-group-addon plus-icon">+</span>
                    <input type="text" id="PhoneExt" class="form-control" ng-dblClick="safeDesignerVm.showTextInpDetails()" numbers-only="true" name="PhoneExt" placeholder="" ng-pattern="/^[0-9]{0,3}$/" maxlength="3" ng-model="$ctrl.parentCtrl.tempObj.Phone.PhoneExt" />
                    <span class="input-group-addon addon-css">-</span>
                    <input type="text" id="Phone1" class="form-control" ng-dblClick="safeDesignerVm.showTextInpDetails()" numbers-only="true" name="phone1" placeholder="" ng-pattern="/^[0-9]{3}$/" maxlength="3" ng-model="$ctrl.parentCtrl.tempObj.Phone.Phone1" required />
                    <span class="input-group-addon addon-css">-</span>
                    <input type="text" id="Phone2" class="form-control" ng-dblClick="safeDesignerVm.showTextInpDetails()"  numbers-only="true" name="phone2" placeholder="" ng-pattern="/^[0-9]{3}$/" maxlength="3" ng-model="$ctrl.parentCtrl.tempObj.Phone.Phone2" required />
                    <span class="input-group-addon addon-css">-</span>
                    <input type="text" id="Phone3" class="form-control" ng-dblClick="safeDesignerVm.showTextInpDetails()" numbers-only="true" name="phone3" placeholder="" ng-pattern="/^[0-9]{4}$/" maxlength="4" ng-model="$ctrl.parentCtrl.tempObj.Phone.Phone3" required />
                  </div>
                </div>`;
            } else if (field.controllerType === 'Fax') {
                inp = `
                <div  class="form-group " draggable="true" onDragstart="angular.element(this).scope().safeDesnCtlr.drag(event)" >
                  <span class="pull-right glyphicon glyphicon-remove delGlph" data-id={{field.id}} ng-click="safeDesignerVm.deleteElements()"></span>
                  <label for="Fax" class="form-label">{{:: 'employer.addemployer.EmployerContact.Fax' |translate}}</label>
                    <div id="Fax" class="input-group" input-phone-directive ng-class="{'has-error':!onFocusFax && $ctrl.parentCtrl.tempObj.Fax.showError &&  ( employerDetailsForm.fax1.$dirty||employerDetailsForm.fax2.$dirty ||  employerDetailsForm.fax3.$dirty)}">
                        <span class="input-group-addon plus-icon">+</span>
                        <input type="text" id="FaxExt" ng-dblClick="safeDesignerVm.showTextInpDetails()" class="form-control" numbers-only="true" name="faxExt" placeholder="" ng-pattern="/^[0-9]{0,3}$/" maxlength="3" ng-model="$ctrl.parentCtrl.tempObj.Fax.FaxExt" />
                        <span class="input-group-addon addon-css">-</span>
                        <input type="text" id="Fax1" ng-dblClick="safeDesignerVm.showTextInpDetails()" class="form-control" numbers-only="true" name="fax1" placeholder="" ng-pattern="/^[0-9]{3}$/" maxlength="3" ng-model="$ctrl.parentCtrl.tempObj.Fax.Fax1" ng-focus="onFocusFax='focusOn'" ng-blur="onFocusFax=''" ng-change="faxValidation($ctrl.parentCtrl.tempObj.Fax)"
                        />
                        <span class="input-group-addon addon-css">-</span>
                        <input type="text" id="Fax2" ng-dblClick="safeDesignerVm.showTextInpDetails()" class="form-control" numbers-only="true" name="fax2" placeholder="" ng-pattern="/^[0-9]{3}$/" maxlength="3" ng-model="$ctrl.parentCtrl.tempObj.Fax.Fax2" ng-focus="onFocusFax='focusOn'" ng-blur="onFocusFax=''" ng-change="faxValidation($ctrl.parentCtrl.tempObj.Fax)"
                        />
                        <span class="input-group-addon addon-css">-</span>
                        <input type="text" id="Fax3" ng-dblClick="safeDesignerVm.showTextInpDetails()" class="form-control" numbers-only="true" name="fax3" placeholder="" ng-pattern="/^[0-9]{4}$/" maxlength="4" ng-model="$ctrl.parentCtrl.tempObj.Fax.Fax3" ng-focus="onFocusFax='focusOn'" ng-blur="onFocusFax=''" ng-change="faxValidation($ctrl.parentCtrl.tempObj.Fax)"
                        />
                    </div>`;
            } else if (field.controllerType === 'boolean') {
                inp = `
                <div class="form-group" draggable="true" onDragstart="angular.element(this).scope().safeDesnCtlr.drag(event)">
                    <div class="form-group-checkbox">
                    <span class="pull-right glyphicon glyphicon-remove delGlph" data-id={{field.id}} ng-click="safeDesignerVm.deleteElements()"></span>
                    
                    <input type="checkbox" name="favoriteColors"  placeholder="Placeholder" id="{{field.id}}" class="form-control" ng-dblClick="safeDesignerVm.showTextInpDetails()" name="test" ng-model="{{field.ngModel}}" /> 
                     <label for="{{field.id}}" class="checkbox-inline form-label" ng-dblClick="safeDesignerVm.showLabelDetails()" style="text-transform:capitalize" ng-class="{checked: location.CanAcceptShipment}">{{field.labelTxt}}</label>   
                    </div>
                </div>
                `;
            } else if (field.controllerType === 'image') {
                inp = `
                    
                    <div class="form-group col-sm-6" draggable="true" onDragstart="angular.element(this).scope().safeDesnCtlr.drag(event)">
                        
                            <div class="col-xs-4">
                            <span class="pull-right glyphicon glyphicon-remove delGlph" data-id={{field.controllerType}} ng-click="safeDesignerVm.deleteElements()"></span>
                             <label for="image" data-id="image" class="form-label"></label>
                                <div class="profile-picture pull-left">
                                    <img draggable="false" ng-src="{{personIdentity ? ((personIdentity.Picture.indexOf('data') != -1 )? personIdentity.Picture :(rctrl.getPersonImageUrl())) : 'images/personnel.png' }}" alt="{{personData.FirstName+ ' '+ personData.LastName}}"  class="person-profile-picture" >
                                    <a href="#" class="edit" title="Edit Profile Photo" ng-click="openCaptureVisitorPictureModal()">Edit Profile Photo</a>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <h3 ng-bind="personData.FirstName+ ' '+ personData.LastName"></h3>
                                <h4  ng-bind="personData.Status|| 'Active'" ng-if="personData.Status" class="status-icon status-icon-active status-label" ng-class="getIconsForStatus(personData.Status)" ></h4>
                            </div>
                        </div>                `;
            }
            // return inp;
            return $interpolate(inp)({ field: field });
        }

    }]);